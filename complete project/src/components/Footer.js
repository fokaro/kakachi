import "./style1.css";
function Footer() {
  return (
    <>
      <body>
        <div class="contenu">
        </div>
        <footer>
          <div class="contenu-footer">
            <div class="bloc footer-services">
              <h3>Nos services</h3>
              <ul class="listes-services">
                <li>
                  <a href="#">Conception de sites webs</a>
                </li>
                <li>
                  <a href="#">Maintenance de site </a>
                </li>
                <li>
                  <a href="#">Machine learning </a>
                </li>
              </ul>
            </div>
            <div class="bloc footer-contacts">
              <h3>Restons en contact</h3>
              <p>Telephone:695047174</p>
              <p>
                Email:<a href="mailto:hege@example.com">takam@yahoo.com</a>
              </p>
              <p>Adresse:Nouvelle route eveche, 6e rue</p>
            </div>
            <div class="bloc footer-horaires">
              <h3>Nos horaires</h3>
              <ul class="listes-horaires">
                <li> ✅ lun 10h-19h</li>
                <li> ✅ mar 10h-19h</li>
                <li> ✅ mer 10h-19h</li>
                <li> ✅ jeu 10h-19h</li>
                <li> ✅ ven 10h-19h</li>
                <li> ❌ sam 10h-19h</li>
                <li> ❌ dim 10h-19h</li>
              </ul>
            </div>
            <div class="bloc footer-medias">
              <h3>Nos Reseaux</h3>
              <ul class="listes-media">
                <li>
                  <a href="#">
                    <img
                      src="facebook.png"
                      alt="icone reseaux sociaux"
                      width="4%"
                      seize="3%"
                    />
                    Facebook
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img
                      src="instagram.jpeg"
                      alt="icone reseaux sociaux"
                      width="4%"
                      seize="3%"
                    />
                    Instagram
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img
                      src="whatsapp.jpeg"
                      alt="icone reseaux sociaux"
                      width="4%"
                      seize="3%"
                    />
                    whattsapp
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </footer>
      </body>
    </>
  );
}
export default Footer;
