import { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";


const DetailsMarchandise = () => {
  const [marchandise, setMarchandise] = useState([]);
  const [name, setName] = useState("");
  const [unit_price, setUnit_price] = useState("");
  const [total_price, setTotal_price] = useState("");
  const [quantity, setQuantity] = useState("");
  const [searchParams] = useSearchParams();
  const [loader, setLoader] = useState("true");
  const navigate = useNavigate();


  useEffect(() => {
    async function fetchData() {
      const marchandiseId = searchParams.get("marchandiseId");
      // You can await here
      let options = {
        method: "POST",
        headers: { "Content-type": "application/json;charset=utf-8" },
        body: JSON.stringify({ id: marchandiseId }),
      };
      const data = await fetch(
        "http://localhost:8001/api/get-one-marchandise",
        options
      ).then((response) => response.json());
      if (data) {
        setLoader(false);
      }
      console.log(searchParams);
      console.log(data);
      setMarchandise(data);
      console.log(data.totalPrice);
      const btn1 = document.getElementById('btn');
      console.log(btn1);
     btn1.addEventListener("click", () => {
      navigate("/MarchandiseTable"); 
    }) 
    }
  
    fetchData();
  }, [searchParams]);

  return (
    <body className="">
      {loader ? (
        <div class="loader"></div>
      ) : (
        <main className="main-content  mt-0">
          <section>
              <div className="container">
                <div className="row">
                  <div className="col-5 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 start-0 text-center justify-content-center flex-column">
                    <div
                      className="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center"
                      style={{
                        backgroundImage: `url('../assets/img/illustrations/illustration-signup.jpg')`,
                        backgroundSize: "cover",
                      }}
                    ></div>
                  </div>
                              <div className="page-header min-vh-100">

                  <div className="col-xl-6 col-md-12 d-flex position-relative top 1 start-0 text-center justify-content-center flex-column ms-auto me-auto ms-lg-auto me-lg-5">
                    <div className="card card-plain">
                      <div className="card-header">
                        <h2 class="text-center">
                          <div class="text-success">DETAIL Marchandise</div>
                        </h2>
                        <div class="container text-center">
                          <label>
                            <div class="text-primary">Name = </div>
                          </label>
                          <label> {marchandise.name}</label>

                          <div>
                            <label>
                              <div class="text-primary">Quantity = </div>
                            </label>
                            <label> {marchandise.quantity}</label>
                          </div>
                          <div>
                            <label>
                              <div class="text-primary">Unit_price = </div>
                            </label>
                            <label> {marchandise.unitPrice}</label>
                          </div>
                          <div>
                            <label>
                              <div class="text-primary">Lass Price= </div>
                            </label>
                            <label> {marchandise.lastPrice}</label>
                          </div>
                          <div>
                            <label>
                              <div class="text-primary">Quantity Sold= </div>
                            </label>
                            <label> {marchandise.quantitySold}</label>
                          </div>

                            <div className="text-center">
                              <div className="row">
                                <div className="text-danger mb-5 text-capitalize ps-3">
                                  <button
                                    id="btn"
                                    type="button"
                                    className="btn btn-lg bg-gradient-success btn-lg w-100 mt-4 mb-0"
                                  >
                                    BACK
                                  </button>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
      )}
    </body>
  );
};
export default DetailsMarchandise;
