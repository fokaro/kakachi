import { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";

const EditerMarchandise = () => {
  const [marchandise, setMarchandise] = useState([]);
  const [name, setName] = useState("");
  const [unitPrice, setUnitPrice] = useState("");
  const [totalPrice, setTotalPrice] = useState("");
  const [quantity, setQuantity] = useState("");
  const [quantitySold, setQuantitySold] = useState("");
  const [lastPrice, setLastPrice] = useState("");

  const [searchParams] = useSearchParams();
  const [loader, setLoader] = useState("true");
  const [id, setId] = useState("");
  const navigate = useNavigate();

  const changeName = (e) => {
    let Name = e.target.value;
    console.log(Name);
    setName(Name);
  };

  const changeUnitPrice = (e) => {
    let unitPrice = e.target.value;
    console.log(unitPrice);
    setUnitPrice(unitPrice);
  };

  const changeLastPrice = (e) => {
    let lastPrice = e.target.value;
    console.log(lastPrice);
    setLastPrice(lastPrice);
  };

  const changeQuantitySold = (e) => {
    let quantitySold = e.target.value;
    console.log(quantitySold);
    setQuantitySold(quantitySold);
  };

  const changeQuantity = (e) => {
    let Quantity = e.target.value;
    console.log(Quantity);
    setQuantity(Quantity);
  };

  const changeMarchandise = async () => {
    const data = {
      id: id,
      name: name,
      quantity: quantity,
      unitPrice: unitPrice,
      totalPrice: totalPrice,
      lastPrice: lastPrice,
      quantitySold: quantitySold,
    };
    console.log(data);
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify(data),
    };
    const valeur = await fetch(
      "http://localhost:8001/api/update-marchandise",
      options
    ).then((response) => response.json());
    if (valeur != null) {
      navigate("/MarchandiseTable");

      console.log("voici le nouvel objet");
      console.log(valeur);
    }
    console.log(totalPrice);
    await updateData();
  };
  const updateData = async () => {
    // You can await here
    const data = await fetch("http://localhost:8001/api/get-marchandise").then(
      (response) => response.json()
    );

    setMarchandise(data);
  };

  useEffect(() => {
    async function fetchData() {
      const marchandiseId = searchParams.get("marchandiseId");
      let options = {
        method: "POST",
        headers: { "Content-type": "application/json;charset=utf-8" },
        body: JSON.stringify({ id: marchandiseId }),
      };
      const data = await fetch(
        "http://localhost:8001/api/get-one-marchandise",
        options
      ).then((response) => response.json());
      setName(data.name);
      setQuantity(data.quantity);
      setTotalPrice(data.totalPrice);
      setUnitPrice(data.unitPrice);
      setLastPrice(data.lastPrice);
      setQuantitySold(data.quantitySold);

      setId(data.id);
      console.log(totalPrice);
    }
    const btn1 = document.getElementById("btn");
    console.log(btn1);
    btn1.addEventListener("click", () => {
      navigate("/MarchandiseTable");
    });
    fetchData();
  }, [searchParams]);

  return (
    <body className="">
      <main className="main-content  mt-0">
        <section>
          <div className="page-header min-vh-100">
            <div className="container">
              <div className="row">
                <div className="col-4 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 start-0 text-center justify-content-center flex-column">
                  <div
                    className="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center"
                    style={{
                      backgroundImage: `url('../assets/img/illustrations/illustration-signup.jpg')`,
                      backgroundSize: "cover",
                    }}
                  ></div>
                </div>
                <div className="col-xl-8 col-lg-5 col-md-7 d-flex flex-column ms-auto me-auto ms-lg-auto me-lg-5">
                  <div className="card card-plain">
                    <div className="card-header">
                      <h1 class="text-center">
                        <div class="text-success">Formulaire d'edition</div>
                      </h1>
                      <br></br>
                      <form role="form"></form>
                      <form role="form">
                        <div className="input-group input-group-outline">
                          <div class="row mb-4">
                            <label
                              For="Name"
                              className="col-sm-5 col-form-label"
                            >
                              Name
                            </label>
                            <div class="col-sm-6">
                              <input
                                readonly
                                class="form-control"
                                type="text"
                                id="name"
                                value={name}
                                onChange={changeName}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="input-group input-group-outline">
                          <div class=" mb-4 row">
                            <label
                              For="Quantity"
                              class="col-sm-5 col-form-label"
                            >
                              Quantity
                            </label>
                            <div class="col-sm-6">
                              <input
                                readonly
                                class="form-control"
                                type="number"
                                id="quantity"
                                value={quantity}
                                onChange={changeQuantity}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="input-group input-group-outline">
                          <div class=" mb-4 row ">
                            <label
                              For="Unit_price"
                              class="col-sm-5 col-form-label"
                            >
                              Unit Price
                            </label>
                            <div class="col-sm-6">
                              <input
                                readonly
                                class="form-control"
                                id="unit_price"
                                type="number"
                                value={unitPrice}
                                onChange={changeUnitPrice}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="input-group input-group-outline">
                          <div class="row mb-4">
                            <label
                              For="lastPrice"
                              class="col-sm-6 col-form-label"
                            >
                              Last Price
                            </label>
                            <div class="col-sm-5">
                              <input
                                readonly
                                class="form-control"
                                id="lastPrice"
                                type="number"
                                value={lastPrice}
                                onChange={changeLastPrice}
                              />
                            </div>
                          </div>
                        </div>
                      
                        
                        <div className="input-group input-group-outline">
                          <div class="row mb-3">
                            <label
                              For="QuantitySold"
                              className="col-sm-6 col-form-label"
                            >
                              Quantity Sold
                            </label>
                            <div class="col-sm-4">
                              <input
                                readonly
                                class="form-control"
                                type="number"
                                id="quantitySold"
                                value={quantitySold}
                                onChange={changeQuantitySold}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="text-center">
                          <button
                            type="button"
                            onClick={changeMarchandise}
                            className="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0"
                          >
                            Cliquez ici
                          </button>
                        </div>
                        <br></br>

                        <div className="text-center">
                          <button
                            id="btn"
                            type="button"
                            className="btn btn-info"
                          >
                            BACK
                          </button>
                        </div>
                        <br></br>
                        <div></div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </body>
  );
};
export default EditerMarchandise;
