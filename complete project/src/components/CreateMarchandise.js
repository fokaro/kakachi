import { useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2'

const CreateMarchandise = () => {
  const [marchandise, setMarchandise] = useState([]);
  const [name, setName] = useState("");
  const [unit_price, setUnit_price] = useState("");
  const [total_price, setTotal_price] = useState("");
  const [quantity, setQuantity] = useState("");
  const [isOpen, setIsOpen] = useState("true");
  const [lastPrice, setLastPrice] = useState("");
  const [quantitySold, setQuantitySold] = useState("");


  const navigate = useNavigate();
 
  
  const changeName = (e) => {
    let Name = e.target.value;
    console.log(Name);
    setName(Name);
  };
  const changeLastPrice = (e) => {
    let lastPrice = e.target.value;
    console.log(lastPrice);
    setLastPrice(lastPrice);
  };

  const changeQuantitySold = (e) => {
    let quantitySold = e.target.value;
    console.log(quantitySold);
    setQuantitySold(quantitySold);
  };

  const changeUnit_price = (e) => {
    let unit_price = e.target.value;
    console.log(unit_price);
    setUnit_price(unit_price);
  };

  const changeTotal_price = (e) => {
    let total_price = e.target.value;
    console.log(total_price);
    setTotal_price(total_price);
  };

  const changeQuantity = (e) => {
    let Quantity = e.target.value;
    console.log(Quantity);
    setQuantity(Quantity);
  };

  const verify = () => {
   let message = ""
   
    if(parseInt(quantitySold) > parseInt(quantity)){

     message = "La quantite vendue ne peut etre superieur a la quantite disponible"

  }
  if(parseInt(unit_price) < parseInt(lastPrice)){
    message = "Le dernier prix ne peut etre superieur au prix unitaire"

  }
return message;
}
const masso = () => {

}



  const createMarchandise = async () => {

    //let decision = verify();

    if(verify() !== ""){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: verify()
        //footer: '<a href="">Why do I have this issue?</a>'
      })

    }else{
    const data = {
      name: name,
      quantity: quantity,
      unit_price: unit_price,
      total_price: total_price,
      lastPrice: lastPrice,
      quantitySold: quantitySold,
    
    };
    
    
   // console.log(data);
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify(data),
    };
    const valeur = await fetch(
      "http://localhost:8001/api/create",
      options
    ).then((response) => response.json());

 console.log("voici la valeur" + valeur);
    if(valeur) {
      //setIsOpen(false);
      Swal.fire({
        icon: 'success',
        title: 'Congratulation...',
        text: 'you have successful create!',
        //footer: '<a href="">Why do I have this issue?</a>'
      })
      navigate("/MarchandiseTable");
  
     
    }

    if(valeur == '0') { 
      Swal.fire({
        icon: 'error',
        //title: 'Oops. "Marchandise already created"  ..',
        text:name + " Marchandise already created " 
        //footer: '<a href="">Why do I have this issue?</a>'
      })
    }
  }
}

  return (
    <body className="">
      {isOpen ? (
        <main className="main-content  mt-0">
          <section>
            <div className="page-header min-vh-100">
              <div className="container">
                <div className="row">
                  <div className="col-4 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 start-0 text-center justify-content-center flex-column">
                    <div
                      className="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center"
                      style={{
                        backgroundImage: `url('../assets/img/illustrations/illustration-signup.jpg')`,
                        backgroundSize: "cover",
                      }}
                    ></div>
                  </div>
                  <div className="col-xl-8 col-lg-5 col-md-7 d-flex flex-column ms-auto me-auto ms-lg-auto me-lg-5">
                    <div className="card card-plain">
                      <div className="card-header">
                        <h2 className="text-success font-weight-bolder">
                          Creation
                        </h2>
                        <p className="mb-0">Enter Your Arcticles</p>
                      </div>
                      <div className="card-body">
                        <form role="form">
                          <div className="input-group input-group-outline mb-3">
                            <label className="form-label">Name</label>
                            <input
                              type="text"
                              className="form-control"
                              aria-label=" Name"
                              onChange={changeName}
                            />
                          </div>
                          <div className="input-group input-group-outline mb-3">
                            <label className="form-label">Quantity</label>
                            <input min="0" 
                            
                              type="number"
                              className="form-control"
                              aria-label=" Quantity"
                              onChange={changeQuantity}
                            />
                          </div>
                          <div className="input-group input-group-outline mb-3">
                            <label className="form-label">Unit Price</label>
                            <input min="0"
                              type="number"
                              className="form-control"
                              aria-label="unit_Price"
                              onChange={changeUnit_price}
                            />
                          </div>
                          <label className="form-label">Total Price</label>

                          <div className="input-group input-group-outline mb-3">
                            <input min="0"
                              type="number"
                              disabled
                              value={unit_price * quantity}
                              className="form-control"
                              //aria-label="total_Price"
                              onChange={changeTotal_price}
                            />
                          </div>
                          <div className="input-group input-group-outline mb-3">
                            <label className="form-label">Last Price</label>
                            <input min="0"
                              type="number"
                              className="form-control"
                              aria-label="lastPrice"
                              onChange={changeLastPrice}
                            />
                          </div>
                          <div className="input-group input-group-outline mb-3">
                            <label className="form-label">quantitySold</label>
                            <input min="0"
                              type="number"
                              className="form-control"
                              aria-label="quantitySold"
                              onChange={changeQuantitySold}
                            />
                          </div>
                          <div className="text-center">
                            <button
                              id="btn1"
                              type="button"
                              className="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0"
                              onClick={createMarchandise}
                              
                            >
                              Click here
                            </button>
                            <br></br>

                            <div className="text-center">
                              <button
                                id="btn"
                                type="button"
                                className="btn btn-info"
                                onClick={() => {
                                  navigate("/MarchandiseTable");
                                }}
                              >
                                BACK
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
      ) : (
        <div class="alert alert-success" role="alert">
          A simple success alert—check it out!
        </div>
      )}
    </body>
  );
};
export default CreateMarchandise;
