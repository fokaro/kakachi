import {
  BrowserRouter as Router,
  Routes,
  Route,
  
} from "react-router-dom";
import Header from "./compenents/Header";
import Home from "./Home";
import Users from "./compenents/Users";
// import About from "./compenents/About";
import Madame from "./compenents/madame";
import './App.css';
import Marchandise from "./compenents/Marchandise";
import Medicament from "./compenents/Medicament";
import Contact from "./compenents/Contact";
import Footer from "./compenents/Footer";
import MarchandiseDetails from "./compenents/MarchandiseDetails";
import MarchandiseEditer from "./compenents/MardichandiseEditer";
import Creation from "./compenents/Creation";

export default function App() {
  return (
    <Router>
        <Routes>
        {/* <Route exact path='/about' element={<About/>}/>*/}
        <Route exact path='/users' element={<Users/>}/>
        <Route exact path='/' element={<Marchandise/>}/> 
        <Route exact path='/madame' element={<Madame/>}/>
        <Route exact path='/marchandise' element={<Marchandise/>}/>
        <Route exact path='/medicament' element={<Medicament/>}/>
        <Route exact path='/header' element={<Header/>}/>
        <Route exact path='/creation' element={<Creation/>}/>
        <Route exact path='/contact' element={<Contact/>}/>
        <Route exact path='/footer' element={<Footer/>}/>
        <Route exact path='/details' element={<MarchandiseDetails/>}/>
        <Route exact path='/editer' element={<MarchandiseEditer/>}/>




        </Routes>
    </Router>
  );

}




