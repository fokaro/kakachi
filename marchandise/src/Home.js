import { useState, useEffect } from "react"
export default function Home(){
    const [professor, setProfesseur]= useState('');
     /* fetch("http://localhost:8000/api/get-professor")
    .then((response)=>{console.log(response.json())})
    .then((responseJson) => {
        // do stuff with responseJSON here...
        console.log(responseJson);
     }); */

     const getApiData = async () => {
        const response = await fetch(
          "http://localhost:8000/api/get-professor"
        ).then((response) => response.json());
      
        // update the state
        console.log(response)
        
        setProfesseur(response);
      };

      useEffect(() => {
        getApiData();
      }, []);
    

      const [allProfesseur, setAllProfesseur]= useState('');

      const getAllProf = async () => {
        const response = await fetch(
          "http://localhost:8000/api/get-allprofesseur"
        ).then((response) => response.json());
      
        // update the state
        console.log(response)
        
        setAllProfesseur(response);
      };

      useEffect(() => {
        getAllProf();
      }, []);


    
    return(
        <> 
        <p>Nous sommes dans home</p><br />
        <p>{professor.name}</p>
        </>
    )}