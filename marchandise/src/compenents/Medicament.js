import { useState,useEffect} from "react"
export default function Medicament() {
       

    const[id, setId]= useState('   ')
    const[name,setName]= useState('');
    const[unit_price,setUnit_price]= useState('');
    const[quantity,setQuantity]= useState('');
    const[medicament,setMedicament]=useState([]);
    const[detailsmedicament, setDetailsMedicament]= useState('');


    const newName= (e)=>{
        let name= e.target.value;
        console.log(name)
        setName(name);
    }
    const newQuantity= (e)=>{
        let quantity= e.target.value;
        console.log(quantity)
        setQuantity(quantity);
    }
    const newUnit_price= (e)=>{
        let unit_price= e.target.value;
        console.log(unit_price)
        setUnit_price(unit_price);
    }

    const createMedocs= async()=>{
    
        const data ={
            'name':name,
            'quantity':quantity,
            'unit_price':unit_price,
        }
        console.log(data)
        let options ={
            method: 'POST',
            headers: { 'Content-type':
                'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)       
        }
        fetch("http://localhost:8000/api/medicament-create",options) 
        .then((response) => response.json());
       await updateData(); 
       
    }
     
    useEffect(() => {
        async function fetchData() {
          // You can await here
          const data = await 
          fetch("http://localhost:8000/api/get-medicament").   
      then((response) => response.json());
  
      console.log (data)
      setMedicament(data);          
        }
        fetchData();
      },[]);



      const details= async(e)=>{
        const id= e.target.value;
        let options ={
            method: 'POST',
            headers: { 'Content-type':
                'application/json;charset=utf-8'
        },
        body: JSON.stringify({'id': id})       
        }
        const data=
          await fetch("http://localhost:8000/api/one-medoc",options) 
        .then((response) => response.json());
        await updateData();
    setDetailsMedicament(data)
    }

    const updateData = async()=>{
        // You can await here
const data = await 
fetch("http://localhost:8000/api/get-medicament").   
then((response) => response.json());

setMedicament(data);          
}
    

const deleteMedicament= async (e)=>{
    const id= e.target.value;
    let options ={
        method: 'POST',
        headers: { 'Content-type':
            'application/json;charset=utf-8'
    },
    body: JSON.stringify({'id': id})       
    }
    await fetch("http://localhost:8000/api/delete-medicament",options) 
    .then((response) => response.json());
    await updateData();
}

const changeMedocs= async()=>{
    
    const data ={
        'id':id,
        'name':name,
        'quantity':quantity,
        'unit_price':unit_price,
    }
    console.log(data)
    let options ={
        method: 'POST',
        headers: { 'Content-type':
            'application/json;charset=utf-8'
    },
    body: JSON.stringify(data)       
    }
    await fetch("http://localhost:8000/api/update-medicament",options) 
    .then((response) => response.json());
    await updateData();
}


    const updateMedicament= async(e)=>{
        const id= e.target.value;
        let options ={
            method: 'POST',
            headers: { 'Content-type':
                'application/json;charset=utf-8'
        },
        body: JSON.stringify({'id': id})       
        }
        const data=
          await fetch("http://localhost:8000/api/one-medoc",options) 
        .then((response) => response.json());
        setName(data.name)
        setQuantity(data.quantity)
        setUnit_price(data.unit_price)
        setId(data.id)
    
    }
    
       



    return (
        <>
        <form> 
             <div>
                <label htmlFor="">name</label>
                <input type="text"  onChange={newName} className="post"/>
             </div>
             <div>
                <label htmlFor="">quantity</label>
                <input type="number" onChange={newQuantity} className="post"/>
             </div>
    
             <div>
                <label htmlFor="">unit_price</label>
                <input type="number" onChange={newUnit_price} className="post"/>
             </div>
             
             <div>
                <button type="button" onClick={createMedocs}>Cliquez ici</button>

             </div>
    
    <div>
             <h2>
                detail d'un medicament
                
            </h2>
          {   <label>name</label> }
            <label>{detailsmedicament.name}</label>
            </div>
            <div>
                <label>quantity</label>
                <label>{detailsmedicament.quantity}</label>
            </div>

            <div>
                <label>prix unitaire</label>
                <label>{detailsmedicament.unit_price}</label>
            </div>


            </form>
<br>
</br>
    <table border='5px'>
       { medicament.map((item)=>
       <tr key={item.id}>
        <td>{item.id}</td>
        <td>{item.name}</td>
        <td>{item.quantity}</td>
        <td>{item.unit_price}</td>
        <td><button type='button' onClick={details} value={item.id}>details</button></td>
        <td><button type='button' onClick={deleteMedicament} value={item.id}>delete</button></td>
        <td><button type='button' onClick={updateMedicament} value={item.id}>editer</button></td>

       </tr>

       )}
    </table>
    <form> 
         <div>
            <label htmlFor="">name</label>
            <input type="text"  value={name} onChange={newName} className="post"/>
         </div>

         <div>
            <label htmlFor="">quantity</label>
            <input type="number" value={quantity} onChange={newQuantity} className="post"/>
         </div>

         <div>
            <label htmlFor="">unit_price</label>
            <input type="number" value={unit_price} onChange={newUnit_price} className="post"/>
         </div>
         
         
         <br>
            </br>
         <div>
            <button type="button" onClick={changeMedocs}>Renommez votre article ici</button>

         </div>
         <br>
            </br>
         <div>
         {/*    <h2>
                DetailsMarchandise
                
            </h2>
            <label>
                name
            </label>
            <label> {detailsMarchandise.name}</label>
         </div>

         <div>
            
            <label>
                quantity
            </label>
            <label> {detailsMarchandise.quantity}</label>
         </div>
         <div>
            
            <label>
                unit_price
            </label>
            <label> {detailsMarchandise.unit_price}</label>
         </div>
         <div>
            
            <label>
                total_price
            </label>
            <label> {detailsMarchandise.total_price}</label> */}
         </div>
         
        </form>
     </>

    ) 
    
    
}