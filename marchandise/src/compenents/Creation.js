import { useState, useEffect } from "react";
import Header from "./Header";
import { Link, useNavigate } from "react-router-dom";
import Footer from "./Footer";

export default function Creation() {
  const [isOpen, setIsOpen] = useState("true");
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [unit_price, setUnit_price] = useState("");
  const [total_price, setTotal_price] = useState("");
  const [quantity, setQuantity] = useState("");
  const [marchandise, setMarchandise] = useState([]);
  const [detailsMarchandise, setDetailsMarchandise] = useState("");
  const navigate = useNavigate();

  const changeName = (e) => {
    let Name = e.target.value;
    console.log(Name);
    setName(Name);
  };

  const changeUnit_price = (e) => {
    let unit_price = e.target.value;
    console.log(unit_price);
    setUnit_price(unit_price);
  };

  const changeTotal_price = (e) => {
    let total_price = e.target.value;
    console.log(total_price);
    setTotal_price(total_price);
  };

  const changeQuantity = (e) => {
    let Quantity = e.target.value;
    console.log(Quantity);
    setQuantity(Quantity);
  };

  const updateData = async () => {
    // You can await here
    const data = await fetch("http://localhost:8000/api/get-marchandise").then(
      (response) => response.json()
    );

    setMarchandise(data);
  };

  const createMarchandise = async () => {
    const data = {
      name: name,
      quantity: quantity,
      unit_price: unit_price,
      total_price: total_price,
    };
    console.log(data);
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify(data),
    };
    const valeur = await fetch(
      "http://localhost:8000/api/create",
      options
    ).then((response) => response.json());
    if (valeur) {
      setIsOpen(false);
      console.log(valeur);
      /*  navigate("/marchandise"); */
    }
  };

  return (
    <>
      <Header />
      {isOpen ? (
        <>
          <div class="p-2 mb-2 bg-success text-white">
            <h1 class="display-2">Formulaire</h1>
          </div>
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
              <div class="text-center"></div>
              <br></br>

              <div class="container text-center">
                <form>
                  <div className="container text-center">
                    <h1 className="btn btn-primary"> Formulaire de création</h1>
                  </div>
                  <br></br>
                  <div class="row mb-3">
                    <label for="Name" class="col-sm-2 col-form-label">
                      Name
                    </label>

                    <div class="col-sm-10">
                      <input
                        type="text"
                        className="form-control"
                        aria-label=" Name"
                        onChange={changeName}
                      />
                    </div>
                  </div>
                  <div class="row mb-3">
                    <label for="Unit_price" class="col-sm-2 col-form-label">
                      Unit_price
                    </label>
                    <div class="col-sm-10">
                      <input
                        type="text"
                        className="form-control"
                        aria-label=" Unit_price"
                        onChange={changeUnit_price}
                      />
                    </div>
                  </div>

                  <div class="row mb-3">
                    <label for="Quantity" class="col-sm-2 col-form-label">
                      Quantity
                    </label>
                    <div class="col-sm-10">
                      <input
                        type="text"
                        className="form-control"
                        aria-label="Quantity"
                        onChange={changeQuantity}
                      />
                    </div>
                  </div>

                  <div class="row mb-3">
                    <label for="Total_price" class="col-sm-2 col-form-label">
                      Total_price
                    </label>
                    <div class="col-sm-10">
                      <input
                        type="text"
                        className="form-control"
                        aria-label="Total_price"
                        onChange={changeTotal_price}
                      />
                    </div>
                  </div>

                  {/*             <label htmlFor="">Quantity</label>
         </div>
</div>
         <div>
         <div class="col-md-6">

{/*             <label htmlFor="">Unit_price</label>
 */}

                  <br></br>
                  <div button class="pull-left">
                    <button
                      type="button"
                      class="btn btn-primary"
                      onClick={createMarchandise}
                    >
                      Cliquez ici
                    </button>
                  </div>
                  <br></br>
                </form>
              </div>
            </div>
          </div>
          {/* </div>
          </div> */}
          <div class="col-md-3"></div>
        </>
      ) : (
        <div class="alert alert-success" role="alert">
          A simple success alert—check it out!
        </div>
      )}
      <Footer />
    </>
  );
}
