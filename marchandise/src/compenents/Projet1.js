import { useState, useEffect } from "react";
import Header from "./Header";
import { Link } from "react-router-dom";


export default function Projet() {
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [unit_price, setUnit_price] = useState("");
  const [total_price, setTotal_price] = useState("");
  const [quantity, setQuantity] = useState("");
  const [marchandise, setMarchandise] = useState([]);
  const [detailsMarchandise, setDetailsMarchandise] = useState("");

  const changeName = (e) => {
    let Name = e.target.value;
    console.log(Name);
    setName(Name);
  };

  const changeUnit_price = (e) => {
    let unit_price = e.target.value;
    console.log(unit_price);
    setUnit_price(unit_price);
  };

  const changeTotal_price = (e) => {
    let total_price = e.target.value;
    console.log(total_price);
    setTotal_price(total_price);
  };

  const changeQuantity = (e) => {
    let Quantity = e.target.value;
    console.log(Quantity);
    setQuantity(Quantity);
  };

  const deleteMarchandise = async (e) => {
    const id = e.target.value;
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify({ id: id }),
    };
    await fetch("http://localhost:8000/api/delete-marchandise", options).then(
      (response) => response.json()
    );
    await updateData();
  };

  const details = async (e) => {
    const id = e.target.value;
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify({ id: id }),
    };
    const data = await fetch(
      "http://localhost:8000/api/get-one-marchandise",
      options
    ).then((response) => response.json());
    await updateData();
    setDetailsMarchandise(data);
  };

  const updateMarchandise = async (e) => {
    const id = e.target.value;
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify({ id: id }),
    };
    const data = await fetch(
      "http://localhost:8000/api/get-one-marchandise",
      options
    ).then((response) => response.json());
    setName(data.name);
    setQuantity(data.quantity);
    setTotal_price(data.total_price);
    setUnit_price(data.unit_price);
    setId(data.id);
  };

  const updateData = async () => {
    // You can await here
    const data = await fetch("http://localhost:8000/api/get-marchandise").then(
      (response) => response.json()
    );

    setMarchandise(data);
  };

  useEffect(() => {
    async function fetchData() {
      // You can await here
      const data = await fetch(
        "http://localhost:8000/api/get-marchandise"
      ).then((response) => response.json());

      console.log(data);
      setMarchandise(data);
    }
    fetchData();
  }, []);

  const createMarchandise = async () => {
    const data = {
      name: name,
      quantity: quantity,
      unit_price: unit_price,
      total_price: total_price,
    };
    console.log(data);
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify(data),
    };
    await fetch("http://localhost:8000/api/create", options).then((response) =>
      response.json()
    );
    await updateData();
  };

  const changeMarchandise = async () => {
    const data = {
      id: id,
      name: name,
      quantity: quantity,
      unit_price: unit_price,
      total_price: total_price,
    };
    console.log(data);
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify(data),
    };
    await fetch("http://localhost:8000/api/update-marchandise", options).then(
      (response) => response.json()
    );
    await updateData();
  };

  return (
    <>
      <Header />
      <div class="text-center">
        <div class="p-2 mb-2 bg-success text-white">
          <h1 class="display-2">Formulaire</h1>
        </div>
      </div>
      <br></br>
      <div className="row">
        <div className="col-md-6">
          <div class="container text-center">
            <form>
              <h2 className="btn btn-primary"> Formulaire de création</h2>
              <br></br>
              <div class="row mb-3">
                <label for="Name" class="col-sm-2 col-form-label">
                  Name
                </label>

                <div class="col-sm-6">
                  <input
                    type="text"
                    className="form-control"
                    aria-label=" Name"
                    onChange={changeName}
                  />
                </div>
              </div>
              <div class="row mb-3">
                <label for="Unit_price" class="col-sm-2 col-form-label">
                  Unit_price
                </label>
                <div class="col-sm-6">
                  <input
                    type="text"
                    className="form-control"
                    aria-label=" Unit_price"
                    onChange={changeUnit_price}
                  />
                </div>
              </div>

              <div class="row mb-3">
                <label for="Quantity" class="col-sm-2 col-form-label">
                  Quantity
                </label>
                <div class="col-sm-6">
                  <input
                    type="text"
                    className="form-control"
                    aria-label="Quantity"
                    onChange={changeQuantity}
                  />
                </div>
              </div>

              <div class="row mb-3">
                <label for="Total_price" class="col-sm-2 col-form-label">
                  Total_price
                </label>
                <div class="col-sm-6">
                  <input
                    type="text"
                    className="form-control"
                    aria-label="Total_price"
                    onChange={changeTotal_price}
                  />
                </div>
              </div>

              {/*             <label htmlFor="">Quantity</label>
         </div>
</div>
         <div>
         <div class="col-md-6">

{/*             <label htmlFor="">Unit_price</label>
 */}

              <br></br>
              <div button class="pull-left">
                <button
                  type="button"
                  class="btn btn-primary"
                  onClick={createMarchandise}
                >
                  Cliquez ici
                </button>
              </div>
              <br></br>
            </form>
          </div>
        </div>
        <div className="col-md-6">
          <div class="container text-center">
            <form>
              <h2 className="btn btn-info">Formulaire d'edition</h2>
              <br></br>
              <div>
                <div class="row mb-3">
                  <label For="Name" class="col-sm-2 col-form-label">
                    Name
                  </label>
                  <div class="col-sm-6">
                    <input
                      class="font-weight-bold"
                      type="text"
                      value={name}
                      onChange={changeName}
                    />
                  </div>
                </div>
              </div>

              <div class="row mb-3">
                <label For="Quantity" class="col-sm-2 col-form-label">
                  Quantity
                </label>
                <div class="col-sm-6">
                  <input
                    class="font-weight-bold"
                    type="number"
                    value={quantity}
                    onChange={changeQuantity}
                  />
                </div>
              </div>

              <div class="row mb-3">
                <label For="Unit_price" class="col-sm-2 col-form-label">
                  Unit_price
                </label>
                <div class="col-sm-6">
                  <input
                    class="font-weight-bold"
                    type="number"
                    value={unit_price}
                    onChange={changeUnit_price}
                  />
                </div>
              </div>
              <div class="row mb-3">
                <label For="Total_price" class="col-sm-2 col-form-label">
                  Total_price
                </label>
                <div class="col-sm-6">
                  <input
                    class="font-weight-bold"
                    type="number"
                    value={total_price}
                    onChange={changeTotal_price}
                  />
                </div>
              </div>
              <br></br>
              <div>
                <button
                  class="btn btn-info"
                  type="button"
                  onClick={changeMarchandise}
                >
                  Cliquez ici
                </button>
              </div>

              <br></br>
              <div>
                {/*    <h2>
                                        DetailsMarchandise
                                        
                                    </h2>
                                    <label>
                                        name
                                    </label>
                                    <label> {detailsMarchandise.name}</label>
                                 </div>
                        
                                 <div>
                                    
                                    <label>
                                        quantity
                                    </label>
                                    <label> {detailsMarchandise.quantity}</label>
                                 </div>
                                 <div>
                                    
                                    <label>
                                        unit_price
                                    </label>
                                    <label> {detailsMarchandise.unit_price}</label>
                                 </div>
                                 <div>
                                    
                                    <label>
                                        total_price
                                    </label>
                                    <label> {detailsMarchandise.total_price}</label> */}
              </div>
            </form>
          </div>
        </div>
      </div>

      <br></br>

      <div className="row">
        <div className="col-md-6">
          <table className="table" border="3px">
            <tr>
              <td>name</td>
              <td>quantite</td>
              <td>unit_price</td>
              <td>price_total</td>
              <td>delete</td>
              <td>detail</td>
              <td>edit</td>
            </tr>
            {marchandise.map((item) => (
              <tr key={item.id}>
                <td>{item.name}</td>
                <td>{item.quantity}</td>
                <td>{item.unit_price}</td>
                <td>{item.total_price}</td>
                <td>
                  <button
                    type="button"
                    onClick={deleteMarchandise}
                    value={item.id}
                  >
                    delete
                  </button>
                </td>
                <td>
                    <Link to={{pathname:"/details",search:"?marchandiseId="+item.id}}>
                  <button type="button" 
                 /*  onClick={details}  */
                 
                  value={item.id}>
                    details
                  </button>
                  </Link>
                </td>
                <td>
                <Link to={{pathname:"/editer",search:"?marchandiseId="+item.id}}>

                  <button
                    type="button"
/*                     onClick={updateMarchandise}
 */                    value={item.id}>
                    editer
                  </button>
                  </Link>
                </td>
              </tr>
            ))}
          </table>
        </div>

        <div className="col-md-6">
          <div>
            <div class="container text-center">
              <h2 class="text-center">
                <div class="text-success">DETAIL Marchandise</div>
              </h2>
              <label>
                <div class="text-primary">Name= </div>
              </label>
              <label> {detailsMarchandise.name}</label>

              <div>
                <label>
                  <div class="text-primary">Quantity=</div>
                </label>
                <label> {detailsMarchandise.quantity}</label>
              </div>
              <div>
                <label>
                  <div class="text-primary">Unit_price=</div>
                </label>
                <label> {detailsMarchandise.unit_price}</label>
              </div>
              <div>
                <label>
                  <div class="text-primary">Total_price=</div>
                </label>
                <label> {detailsMarchandise.total_price}</label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br></br>
    </>
  );
}
