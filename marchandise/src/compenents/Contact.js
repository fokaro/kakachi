import Footer from "./Footer";
import Header from "./Header";

function Contact() {
  return (
    <>
      <Header />

      <img
        src="contact.jpg"
        alt="Photo de montagne"
        width="100%"
        seize="100%"
      />
      
   <Footer/>
    </>
  );
}
export default Contact;
