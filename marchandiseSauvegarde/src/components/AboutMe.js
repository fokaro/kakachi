
import React, { useState } from 'react';
function AboutMe(){
    
    const [count, setCount] = useState(0);
    return(
<div>
      <p>Vous avez cliqué {count} fois</p>
      <button onClick={() => setCount(count + 2)}>
        Cliquez ici
      </button>
    </div>  )
}
export default AboutMe;